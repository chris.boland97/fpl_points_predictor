# Fantasy Premier League Player Points Predictor


## Summary

Dominate your mini-leagues! The power of footballing data in the palm of your hand!

Pass in your Fantasy Premier League manager_id and receive a prediction of the number of points each of your players will get in the upcoming Game Week.

Make an informed decision on who to bench, sell, or captain. 


## Setup

- [ ] Clone the repository.
- [ ] A local environment can be created from requirements.txt
- [ ] create database to store requests and responses

```
python build_database.py
```

- [ ] Navigate to the project directory on the command line and set up flask with commands below.

```
export FLASK_APP=main.py
export FLASK_ENV=development
```

- [ ] Run the app

```
flask run
```

It is possible to specify the exact port to host:

```
flask run --port=<port_number>
```

## Making Requests

The app has a single endpoint that requests can be made to:

* /fpl/<manager_id>

When making requests, be sure to pass in the manager_id of your team. This can be found by navigating to the Fantasy Premier League website: https://fantasy.premierleague.com

If you already have an account and are logged in, navigate to the "Points" tab. Once here, you can find your manager_id in the URL as shown in this screenshot below:
![image info](images/url_example.png)

If you do not have a Fantasy Premier League team, some IDs that can be used to test the application are included below:
 - 2184895 (https://fantasy.premierleague.com/entry/2184895/event/21)
 - 4221225 (https://fantasy.premierleague.com/entry/4221225/event/21)
 - 6354663 (https://fantasy.premierleague.com/entry/6354663/event/21)
 - 3078245 (https://fantasy.premierleague.com/entry/3078245/event/21)
 - 635393 (https://fantasy.premierleague.com/entry/635393/event/21)

Software such as Postman (https://www.postman.com/) can be used to make requests to the endpoint once the application is running locally.

Data is returned in JSON format. Players in your team are listed along with their predicted total points for the next Game Week.
![image info](images/request_example.png)

If a player has 0 chance to play in the next match (e.g., if they are injured or suspended) they are assigned 0 points.

## Accessing the Database

The database is implemented with sqlite3.

It can be accessed from the command line. 

 - [ ] Navigate to the project root directory
 - [ ] Run the command below

 ```
 sqlite3 fpl_database.db
 ```

  - [ ] Logs can be accessed from the requests_log table:
  ```
  SELECT * FROM requests_log;
  ```

  This will return the:
  - UID of the entry
  - Team ID sent in the request
  - Per-player points predictions sent in the response
