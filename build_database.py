import sqlite3

def create_connection(db_file):
    conn = None
    try:
        conn = sqlite3.connect(db_file)
        return conn
    except sqlite3.Error as error:
        print(error)

    return conn

def create_table(conn):
    try:
        c = conn.cursor()
        c.execute(
            """ CREATE TABLE IF NOT EXISTS requests_log (
                id INTEGER PRIMARY KEY,
                team_id INTEGER NOT NULL,
                predictions TEXT NOT NULL,
                timestamp DATETIME DEFAULT CURRENT_TIMESTAMP); 
            """
            )
    except sqlite3.Error as error:
        print(error)

def main():
    database = "fpl_database.db"
    conn = create_connection(database)
    if conn is not None:
        print('Connected!')
        create_table(conn)
        conn.close()
    else:
        print("Error! Cannot create the database connection.")

if __name__ == '__main__':
    main()