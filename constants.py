ROLLING_FEATURES = ['assists', 'clean_sheets', 'creativity', 'goals_conceded', 'goals_scored', 'ict_index', 'influence', 'minutes', 'saves', 'threat']
SCALING_FEATURES = ['assists', 'clean_sheets', 'creativity', 'goals_conceded', 'goals_scored', 'ict_index', 'influence', 'minutes', 'saves', 'threat', 'difficulty_difference']
INPUT_FEATURES = ['assists_avg', 'clean_sheets_avg', 'creativity_avg', 'difficulty_difference', 'goals_conceded_avg', 'goals_scored_avg', 'ict_index_avg', 'influence_avg', 'minutes_avg', 'saves_avg', 'threat_avg', 'is_home']
DATABASE = 'fpl_database.db'