import requests
import pandas as pd


def get_summary():
    summary_url = 'https://fantasy.premierleague.com/api/bootstrap-static/'
    response = requests.get(summary_url)
    return response.json()


def get_team_overview(team_id):
    # get the current GW
    summary_df = pd.DataFrame(get_summary()['events'])
    event = summary_df.query('is_current==True')['id'].iloc[0]

    # get team data
    team_data_url = f'https://fantasy.premierleague.com/api/entry/{team_id}/event/{event}/picks/'
    response = requests.get(team_data_url)
    team_data = pd.DataFrame(response.json()['picks'])
    
    return team_data


def get_prior_and_upcoming_fixtures(player_id):
    url  = f'https://fantasy.premierleague.com/api/element-summary/{player_id}/'
    response = requests.get(url)
    upcoming_fixtures = pd.DataFrame(response.json()['fixtures'])
    history = pd.DataFrame(response.json()['history'])

    return upcoming_fixtures, history

    

def get_fixture_data(event_id, fixture_id):
    url = f'https://fantasy.premierleague.com/api/fixtures/?event={event_id}'
    response = requests.get(url)
    fixture_df = pd.DataFrame(response.json())
    fixture = fixture_df.query(f'id == {fixture_id}')
    return fixture