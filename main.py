from flask import Flask, g
from getters import get_team_overview
from utils import get_player_details, get_predicted_points
import sqlite3
import json
from constants import DATABASE

app = Flask(__name__)

def get_db():
    return sqlite3.connect(DATABASE)


def log_request(team_id, predictions):
    db = get_db()
    db.execute('INSERT INTO requests_log (team_id, predictions) VALUES (?, ?)',
               (team_id, predictions))
    db.commit()
    db.close()

@app.route("/fpl/<int:team_id>")
def hello_world(team_id):
    predictions = {}
    team_data = get_team_overview(team_id)
    for id, row in team_data.iterrows():
        player_id = row['element']
        name, can_play = get_player_details(player_id)
        if can_play:
            points_prediction = get_predicted_points(player_id)
            predictions.update({name : round(points_prediction, 3)})
        else:
            predictions.update({name : 0})
    
    predictions = json.dumps(predictions, ensure_ascii=False).encode('utf8')
    log_request(team_id, predictions)
    return predictions