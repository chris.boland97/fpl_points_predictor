from constants import ROLLING_FEATURES, SCALING_FEATURES, INPUT_FEATURES
import joblib
import pandas as pd
import numpy as np
from getters import get_summary, get_prior_and_upcoming_fixtures, get_fixture_data


def build_model_input(player_history, upcoming_fixture, next_fixture_is_home):
    scaler = joblib.load('data/minmax_scaler.pkl')
    
    if next_fixture_is_home == 1:
        player_history['difficulty_difference'] = upcoming_fixture['team_h_difficulty'].iloc[0] - upcoming_fixture['team_a_difficulty'].iloc[0]
    else:
        player_history['difficulty_difference'] = upcoming_fixture['team_a_difficulty'].iloc[0] - upcoming_fixture['team_h_difficulty'].iloc[0]
    
    player_history[SCALING_FEATURES] = scaler.transform(player_history[SCALING_FEATURES])
    player_history = player_history[SCALING_FEATURES].astype(float).mean().to_frame().T
    
    data_instance = player_history[ROLLING_FEATURES]
    data_instance.rename(lambda x: f'{x}_avg', axis=1, inplace=True)
    data_instance['difficulty_difference'] = player_history['difficulty_difference']
    data_instance['is_home'] = next_fixture_is_home

    return data_instance


def inference(model_input):
    model_input = model_input[INPUT_FEATURES]
    model = joblib.load('models/lasso_regression_model.pkl')
    out = model.predict(model_input)
    return np.float64(out[0])


def get_player_details(player_id):
    df = pd.DataFrame(get_summary()['elements'])
    player_summary = df.query(f'id == {player_id}')
    can_play = player_summary['chance_of_playing_next_round'].iloc[0] != 0
    player_name = f"{player_summary['first_name'].iloc[0]} {player_summary['second_name'].iloc[0]}"

    return player_name, can_play


def get_recent_form_and_upcoming_fixture_details(player_id):
    upcoming_fixtures, history = get_prior_and_upcoming_fixtures(player_id)

    next_fixture = upcoming_fixtures.sort_values(by='kickoff_time').iloc[0]
    next_event_id, next_fixture_id, next_fixture_is_home = int(next_fixture['event']), int(next_fixture['id']), next_fixture['is_home'].astype(int)
    recent_history = history.sort_values(by='kickoff_time', ascending=False).head()

    return next_event_id, next_fixture_id, next_fixture_is_home, recent_history


def get_predicted_points(player_id):
    upcoming_event_id, upcoming_fixture_id, next_fixture_is_home, history = get_recent_form_and_upcoming_fixture_details(player_id)
    upcoming_fixture_data = get_fixture_data(upcoming_event_id, upcoming_fixture_id)
    model_input = build_model_input(history, upcoming_fixture_data, next_fixture_is_home)
    
    return inference(model_input)
